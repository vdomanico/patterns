package it.gof.wolf.patterns.creational.singleton;

import it.gof.wolf.patterns.creational.singleton.impl.LazySingleton;
import it.gof.wolf.patterns.creational.singleton.impl.Singleton;

public class App {

    public static void main(String[] args) {

        Singleton singleton1;
        Singleton singleton2;
        LazySingleton lazySingleton1;
        LazySingleton lazySingleton2;

        System.out.println("Ready to invoke Singleton.getSingleton()");
        singleton1 = Singleton.getSingleton();
        singleton2= Singleton.getSingleton();
        System.out.println(singleton1);
        System.out.println(singleton2);

        System.out.println("Ready to invoke LazySingleton.getLazySingleton()");
        lazySingleton1 = LazySingleton.getLazySingleton();
        lazySingleton2 = LazySingleton.getLazySingleton();
        System.out.println(lazySingleton1);
        System.out.println(lazySingleton2);


    }

}
