package it.gof.wolf.patterns.creational.singleton.impl;

public class Singleton {

    private static final Singleton SINGLETON;
    static {
        System.out.println("Calling new Singleton()");
        SINGLETON = new Singleton();
    }

    private Singleton(){}

    public static Singleton getSingleton() {
        return SINGLETON;
    }
}
