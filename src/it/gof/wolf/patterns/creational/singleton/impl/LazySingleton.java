package it.gof.wolf.patterns.creational.singleton.impl;

public class LazySingleton {

    private LazySingleton(){ }

    private static class LazyHolder {
        static final LazySingleton INSTANCE;
        static {
            System.out.println("Calling new LazySingleton()");
            INSTANCE = new LazySingleton();
        }
    }

    public static LazySingleton getLazySingleton() {
        System.out.println("Calling getLazySingleton()");
        return LazyHolder.INSTANCE;
    }
}
