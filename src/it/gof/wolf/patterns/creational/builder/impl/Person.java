package it.gof.wolf.patterns.creational.builder.impl;

public class Person {

    private String firstName;
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public static PersonBuilder builder(){
        return new PersonBuilder();
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }

    public static class PersonBuilder{

        private final Person person;

        public PersonBuilder(){
            this.person = new Person();
        }

        public Person build(){
            return this.person;
        }

        public PersonBuilder firstName(String firstName){
            this.person.firstName = firstName;
            return this;
        }

        public PersonBuilder lastName(String lastName){
            this.person.lastName = lastName;
            return this;
        }

    }

}
