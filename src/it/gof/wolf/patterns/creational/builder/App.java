package it.gof.wolf.patterns.creational.builder;

import it.gof.wolf.patterns.creational.builder.impl.Person;

public class App {

    public static void main(String[] args) {

        Person person = Person
                .builder()
                .firstName("Will")
                .lastName("Smith")
                .build();
        System.out.println(person);
    }

}
