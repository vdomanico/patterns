package it.gof.wolf.patterns.structural.decorator.impl;

public class SicilianPizza extends Consumption {

    @Override
    public String getName() {
        return "Sicilian Pizza";
    }

    @Override
    public Double getPrice() {
        return 5.0;
    }
}
