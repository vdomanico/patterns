package it.gof.wolf.patterns.structural.decorator.impl;

public class BaconExtraConsumptionDecorator extends ExtraConsumptionDecorator{

    public BaconExtraConsumptionDecorator(Consumption consumption){
        super(consumption);
    }

    @Override
    public String getName() {
        return consumption.getName() + " + Bacon";
    }

    @Override
    public Double getPrice() {
        return consumption.getPrice() + 2.5;
    }
}
