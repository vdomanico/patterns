package it.gof.wolf.patterns.structural.decorator.impl;

public abstract class Consumption {

    public abstract String getName();
    public abstract Double getPrice();

}
