package it.gof.wolf.patterns.structural.decorator.impl;

public class GrilledPizza extends Consumption {

    @Override
    public String getName() {
        return "Grilled Pizza";
    }

    @Override
    public Double getPrice() {
        return 5.5;
    }

}
