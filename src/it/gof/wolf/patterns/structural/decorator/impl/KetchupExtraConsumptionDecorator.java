package it.gof.wolf.patterns.structural.decorator.impl;

public class KetchupExtraConsumptionDecorator extends ExtraConsumptionDecorator{

    public KetchupExtraConsumptionDecorator(Consumption consumption){
        super(consumption);
    }

    @Override
    public String getName() {
        return consumption.getName() + " + Ketchup";
    }

    @Override
    public Double getPrice() {
        return consumption.getPrice() + 1.5;
    }
}
