package it.gof.wolf.patterns.structural.decorator.impl;

public abstract class ExtraConsumptionDecorator extends Consumption {

    protected final Consumption consumption;

    protected ExtraConsumptionDecorator(Consumption consumption) {
        this.consumption = consumption;
    }
}
