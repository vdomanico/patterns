package it.gof.wolf.patterns.structural.decorator.impl;

public class HamExtraConsumptionDecorator extends ExtraConsumptionDecorator{

    public HamExtraConsumptionDecorator(Consumption consumption){
        super(consumption);
    }

    @Override
    public String getName() {
        return consumption.getName() + " + Ham";
    }

    @Override
    public Double getPrice() {
        return consumption.getPrice() + 2.5;
    }
}
