package it.gof.wolf.patterns.structural.decorator;

import it.gof.wolf.patterns.structural.decorator.impl.*;

import java.util.List;

public class App {

    public static void main(String[] args) {

        App app = new App();

        SicilianPizza sicilianPizza = new SicilianPizza();
        GrilledPizza grilledPizza = new GrilledPizza();

        BaconExtraConsumptionDecorator baconExtraConsumptionDecorator = new BaconExtraConsumptionDecorator(sicilianPizza);
        HamExtraConsumptionDecorator hamExtraConsumptionDecorator = new HamExtraConsumptionDecorator(grilledPizza);
        KetchupExtraConsumptionDecorator ketchupExtraConsumptionDecorator = new KetchupExtraConsumptionDecorator(hamExtraConsumptionDecorator);

        app.showOrders(List.of(baconExtraConsumptionDecorator, hamExtraConsumptionDecorator, ketchupExtraConsumptionDecorator));

    }

    public void showOrders(List<Consumption> consumptionList){
        for(Consumption consumption : consumptionList){
            System.out.println(consumption.getName() + ": " + consumption.getPrice());
        }
    }

}
