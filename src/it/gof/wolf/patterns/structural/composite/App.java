package it.gof.wolf.patterns.structural.composite;

import it.gof.wolf.patterns.structural.composite.impl.Directory;
import it.gof.wolf.patterns.structural.composite.impl.File;

public class App {

    public static void main(String[] args) {

        App app = new App();

        Directory homeDirectory = new Directory("/home");
        Directory adminDirectory = new Directory("/admin");
        File file = new File("coolPrank.mp4");
        adminDirectory.add(homeDirectory);
        homeDirectory.add(file);
        app.printDirectoryTree(adminDirectory);

    }


    public void printDirectoryTree(Directory directory){
        directory.print();
    }
}
