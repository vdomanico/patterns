package it.gof.wolf.patterns.structural.composite.impl;

public class File implements FileSystem {

    private final String fileName;

    public File(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void add(FileSystem fileSystem) {
        System.out.println("Cannot add fileSystem to File");
    }

    @Override
    public void remove(FileSystem fileSystem) {
        System.out.println("Cannot remove fileSystem to File");
    }

    @Override
    public void print() {
        System.out.println("File name is: " + fileName);
    }
}
