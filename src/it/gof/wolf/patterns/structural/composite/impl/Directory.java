package it.gof.wolf.patterns.structural.composite.impl;

import java.util.ArrayList;
import java.util.List;

public class Directory implements FileSystem {

    private final String directoryName;
    private final List<FileSystem> directory;

    public Directory(String directoryName) {
        this.directoryName = directoryName;
        this.directory = new ArrayList<>();
    }

    @Override
    public void add(FileSystem fileSystem) {
        directory.add(fileSystem);
    }

    @Override
    public void remove(FileSystem fileSystem) {
        directory.remove(fileSystem);
    }

    @Override
    public void print() {
        System.out.println("Inside " + directoryName + " Directory");
        for(FileSystem fileSystem : directory){
            fileSystem.print();
        }
    }
}
