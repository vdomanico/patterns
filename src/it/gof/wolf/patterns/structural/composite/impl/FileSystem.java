package it.gof.wolf.patterns.structural.composite.impl;

public interface FileSystem {

    void add(FileSystem fileSystem);
    void remove(FileSystem fileSystem);
    void print();

}
