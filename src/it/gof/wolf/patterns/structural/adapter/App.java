package it.gof.wolf.patterns.structural.adapter;

import it.gof.wolf.patterns.structural.adapter.impl.*;

import java.util.List;

public class App {

    public static void main(String[] args) {

        App app = new App();

        WindowsCalculator windowsCalculator = new WindowsCalculator("WIN10 is powerful!");
        MacCalculator macCalculator = new MacCalculator("OS X Lion is powerful!");
        PascalCalculator pascalCalculator = new PascalCalculator(100);
        AdapterCalculator adapterCalculator = new AdapterCalculator(pascalCalculator);

        List<Calculator> calculatorList = List.of(windowsCalculator, macCalculator, adapterCalculator);
        app.showsCalculator(calculatorList);
    }

    public void showsCalculator(List<Calculator> calculators){
        for (Calculator calculator : calculators){
            calculator.calculate();
        }
    }

}
