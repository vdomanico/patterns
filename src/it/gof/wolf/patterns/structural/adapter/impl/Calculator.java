package it.gof.wolf.patterns.structural.adapter.impl;

public interface Calculator {

    void calculate();

}
