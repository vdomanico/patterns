package it.gof.wolf.patterns.structural.adapter.impl;

public class AdapterCalculator implements Calculator {

    public final PrimitiveCalculator primitiveCalculator;

    public AdapterCalculator(PrimitiveCalculator primitiveCalculator) {
        this.primitiveCalculator = primitiveCalculator;
    }

    @Override
    public void calculate() {
        this.primitiveCalculator.calculate();
    }
}
