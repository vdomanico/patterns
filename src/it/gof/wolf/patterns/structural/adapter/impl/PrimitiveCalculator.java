package it.gof.wolf.patterns.structural.adapter.impl;

public interface PrimitiveCalculator {

    void calculate();

}
