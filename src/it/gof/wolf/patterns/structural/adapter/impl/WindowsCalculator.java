package it.gof.wolf.patterns.structural.adapter.impl;

public class WindowsCalculator implements Calculator {

    private final String message;

    public WindowsCalculator(String message) {
        this.message = message;
    }

    @Override
    public void calculate() {
        System.out.println("WINDOWS:" + message);
    }
}
