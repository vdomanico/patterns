package it.gof.wolf.patterns.structural.adapter.impl;

public class PascalCalculator implements PrimitiveCalculator {

    private final Integer integer;

    public PascalCalculator(Integer integer) {
        this.integer = integer;
    }

    @Override
    public void calculate() {
        System.out.println("PASCAL:" + integer);
    }
}
