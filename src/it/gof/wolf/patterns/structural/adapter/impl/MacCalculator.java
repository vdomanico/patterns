package it.gof.wolf.patterns.structural.adapter.impl;

public class MacCalculator implements Calculator {

    private final String message;

    public MacCalculator(String message) {
        this.message = message;
    }

    @Override
    public void calculate() {
        System.out.println("MAC:" + message);
    }
}
