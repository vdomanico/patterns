package it.gof.wolf.patterns.structural.facade;

public class App {

    public static void main(String[] args) {

        CarFacade carFacade = new CarFacade();
        carFacade.start();

    }

}
