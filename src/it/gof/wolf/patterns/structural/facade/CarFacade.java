package it.gof.wolf.patterns.structural.facade;

public class CarFacade {

    public void start() {
        System.out.println("Engine started");
        System.out.println("Car moving");
    }
}
