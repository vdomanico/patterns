package it.gof.wolf.patterns.structural.flyweight;

import it.gof.wolf.patterns.structural.flyweight.impl.Car;
import it.gof.wolf.patterns.structural.flyweight.impl.VehicleFactory;

import java.awt.*;

public class App {

    public static void main(String[] args) {
        Car blackCar1 = (Car) VehicleFactory.getVehicle(Color.BLACK);
        Car blackCar2 = (Car) VehicleFactory.getVehicle(Color.BLACK);
        Car blackCar3 = (Car) VehicleFactory.getVehicle(Color.BLACK);
        Car redCar1 = (Car) VehicleFactory.getVehicle(Color.RED);
        Car redCar2 = (Car) VehicleFactory.getVehicle(Color.RED);

        System.out.println(blackCar1.toString());
        System.out.println(blackCar2.toString());
        System.out.println(blackCar3.toString());
        System.out.println(redCar1.toString());
        System.out.println(redCar2.toString());
    }
}
