package it.gof.wolf.patterns.structural.flyweight.impl;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class VehicleFactory {

    private static final Map<Color, Vehicle> cachedMap;
    static {
        cachedMap = new HashMap<>();
    }

    public static Vehicle getVehicle(Color color){

        return cachedMap.computeIfAbsent(color,
                (c) -> new Car("Car -> " + UUID.randomUUID().toString(), c));
    }
}
