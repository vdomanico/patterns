package it.gof.wolf.patterns.structural.flyweight.impl;

import java.awt.*;

public class Car extends Vehicle {

    private final String name;

    public Car(String name, Color color) {
        this.name = name;
        this.color = color;
    }

    @Override
    public String toString() {
        return "Car {" +
                "name='" + name + '\'' +
                ", color=" + color +
                '}';
    }
}
