package it.gof.wolf.patterns.structural.bridge.impl;

public interface Color {

    String fill();

}
