package it.gof.wolf.patterns.structural.bridge.impl;

public class Circle extends Shape {

    public Circle(Color color){
        this.color = color;
    }

    @Override
    public void draw() {
        System.out.println(color.fill() + " Circle");
    }
}
