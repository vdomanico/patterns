package it.gof.wolf.patterns.structural.bridge.impl;

public class RedColor implements Color {

    @Override
    public String fill() {
        return "Red";
    }
}
