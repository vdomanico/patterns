package it.gof.wolf.patterns.structural.bridge.impl;

public class BluColor implements Color {

    @Override
    public String fill() {
        return "Blue";
    }
}
