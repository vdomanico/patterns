package it.gof.wolf.patterns.structural.bridge.impl;

public class Square extends Shape {

    public Square(Color color){
        this.color = color;
    }

    @Override
    public void draw() {
        System.out.println(color.fill() + " Square");
    }
}
