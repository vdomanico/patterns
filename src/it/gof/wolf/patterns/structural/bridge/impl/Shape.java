package it.gof.wolf.patterns.structural.bridge.impl;

public abstract class Shape {

    protected Color color;
    public abstract void draw();

}
