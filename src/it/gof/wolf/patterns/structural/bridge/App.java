package it.gof.wolf.patterns.structural.bridge;

import it.gof.wolf.patterns.structural.bridge.impl.*;

import java.util.List;

public class App {

    public static void main(String[] args) {

        App app = new App();

        RedColor redColor = new RedColor();
        BluColor bluColor = new BluColor();
        Circle circle = new Circle(redColor);
        Square square = new Square(bluColor);

        app.drawShapes(List.of(circle, square));
    }

    public void drawShapes(List<Shape> shapeList){
        for(Shape shape : shapeList){
            shape.draw();
        }
    }

}
