package it.gof.wolf.patterns.structural.proxy;

import it.gof.wolf.patterns.structural.proxy.impl.IProcessor;
import it.gof.wolf.patterns.structural.proxy.impl.ProcessorDynamicProxy;
import it.gof.wolf.patterns.structural.proxy.impl.ProcessorImpl;
import it.gof.wolf.patterns.structural.proxy.impl.ProcessorSimpleProxy;

import java.lang.reflect.Proxy;

public class App {

    public static void main(String[] args) {

        IProcessor simpleProxyProcessor = new ProcessorSimpleProxy( () -> System.out.println("Processing") );
        simpleProxyProcessor.process();

        IProcessor dynamicProxyProcessor = (IProcessor) Proxy.newProxyInstance(IProcessor.class.getClassLoader(),
                new Class[]{IProcessor.class},
                new ProcessorDynamicProxy(new ProcessorImpl()));
        dynamicProxyProcessor.process();

    }

}
