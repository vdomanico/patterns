package it.gof.wolf.patterns.structural.proxy.impl;

public class ProcessorImpl implements IProcessor {

    public ProcessorImpl() {
        System.out.println("Process initialization");
    }

    @Override
    public void process() {
        System.out.println("Processing... Done");
    }

}
