package it.gof.wolf.patterns.structural.proxy.impl;

@FunctionalInterface
public interface IProcessor {

    void process();

}
