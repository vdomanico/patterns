package it.gof.wolf.patterns.structural.proxy.impl;

public class ProcessorSimpleProxy implements IProcessor {

    private final IProcessor iProcessor;

    public ProcessorSimpleProxy(IProcessor iProcessor) {
        this.iProcessor = iProcessor;
    }

    @Override
    public void process() {
        System.out.println("Before process");
        iProcessor.process();
        System.out.println("After process");
    }
}
