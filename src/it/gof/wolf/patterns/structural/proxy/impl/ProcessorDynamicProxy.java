package it.gof.wolf.patterns.structural.proxy.impl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class ProcessorDynamicProxy implements InvocationHandler {

    private final IProcessor iProcessor;

    public ProcessorDynamicProxy(IProcessor iProcessor) {
        this.iProcessor = iProcessor;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("Before Processing");
        Object object = method.invoke(iProcessor, args);
        System.out.println("After Processing");
        return object;
    }
}
