package it.gof.wolf.patterns.behavioral.visitor;

import it.gof.wolf.patterns.behavioral.visitor.impl.*;

import java.util.List;

public class App {

    public static void main(String[] args) {

        App app = new App();
        VisitableSilverItem silverItem = new VisitableSilverItem("SIT", "Silver ring", 200.000);
        VisitableGoldItem goldItem = new VisitableGoldItem("GIT", "Golder ring", 800.000);
        double amount = app.getTotalAmount(new ShoppingVisitor(), List.of(silverItem, goldItem));
        System.out.println(amount);

    }

    public double getTotalAmount(Visitor<Item, Double> visitor, List<Visitable<Item, Double>> visitableList){

        double amount = 0.0;

        for(Visitable<Item, Double> visitable : visitableList){
            amount += visitable.accept(visitor);
        }

        return amount;
    }
}
