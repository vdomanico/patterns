package it.gof.wolf.patterns.behavioral.visitor.impl;

public interface Visitor<T, R> {

    R visit(T t);

}
