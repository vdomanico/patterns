package it.gof.wolf.patterns.behavioral.visitor.impl;

public class ShoppingVisitor implements Visitor<Item, Double>{

    @Override
    public Double visit(Item item) {
        return item.getPrice();
    }

}
