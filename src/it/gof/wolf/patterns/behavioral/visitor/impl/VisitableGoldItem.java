package it.gof.wolf.patterns.behavioral.visitor.impl;

public class VisitableGoldItem extends Item implements Visitable<Item, Double> {

    public VisitableGoldItem(String code, String description, Double price) {
        super(code, description, price);
    }

    @Override
    public Double accept(Visitor<Item, Double> visitor) {
        return visitor.visit(this);
    }
}
