package it.gof.wolf.patterns.behavioral.visitor.impl;

public interface Visitable<T, R> {

    R accept(Visitor<T, R> visitor);

}
