package it.gof.wolf.patterns.behavioral.visitor.impl;

public class VisitableSilverItem extends Item implements Visitable<Item, Double> {

    public VisitableSilverItem(String code, String description, Double price) {
        super(code, description, price);
    }

    @Override
    public Double accept(Visitor<Item, Double> visitor) {
        return visitor.visit(this);
    }
}
