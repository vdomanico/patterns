package it.gof.wolf.patterns.behavioral.visitor.impl;

public abstract class Item {

    protected String code;
    protected String description;
    protected Double price;
    protected Item(String code, String description, Double price){
        this.code = code;
        this.description = description;
        this.price = price;
    }
    protected Double getPrice(){
        return this.price;
    };

}
