package it.gof.wolf.patterns.behavioral.template;

import it.gof.wolf.patterns.behavioral.template.impl.JavaExecutor;

public class App {

    public static void main(String[] args) {
        new JavaExecutor().execute();
    }

}
