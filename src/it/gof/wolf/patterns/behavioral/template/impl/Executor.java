package it.gof.wolf.patterns.behavioral.template.impl;

public abstract class Executor {

    protected abstract void beforeExecution();
    protected abstract void afterExecution();
    protected abstract void doExecute();

    public void execute(){
        beforeExecution();
        doExecute();
        afterExecution();
    }

}
