package it.gof.wolf.patterns.behavioral.template.impl;

public class JavaExecutor extends Executor{

    @Override
    protected void beforeExecution() {
        System.out.println("Before Execution");
    }

    @Override
    protected void afterExecution() {
        System.out.println("After Execution");
    }

    @Override
    protected void doExecute() {
        System.out.println("Executing... ");
    }
}
