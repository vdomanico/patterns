package it.gof.wolf.patterns.behavioral.memento.impl;

public class Originator {

    private Object state;

    public Originator(Object state){ this.state = state; }

    public Memento createMemento(){
        return new Memento(state);
    }

    public void restoreMemento(Memento memento){
        this.state = memento.getState();
    }

    public void setState(Object state) {
        this.state = state;
    }

    public Object getState(){
        return state;
    }

}
