package it.gof.wolf.patterns.behavioral.memento;

import it.gof.wolf.patterns.behavioral.memento.impl.Memento;
import it.gof.wolf.patterns.behavioral.memento.impl.Originator;

public class App {

    public static void main(String[] args) {

        Originator originator = new Originator("x");
        System.out.println("Originator state: " + originator.getState());
        Memento xMemento = originator.createMemento();
        originator.setState("y");
        originator.setState("z");
        System.out.println("Originator state: " + originator.getState());
        originator.restoreMemento(xMemento);
        System.out.println("Originator state: " + originator.getState());
    }

}
