package it.gof.wolf.patterns.behavioral.interpreter;

import it.gof.wolf.patterns.behavioral.interpreter.impl.*;

public class App {

    public static void main(String[] args) {
        InterpreterEngine interpreterEngine = new InterpreterEngine();
        AddExpression addExpression = new AddExpression("3  4");
        SubExpression subExpression = new SubExpression("5  4");
        MulExpression mulExpression = new MulExpression("5     5");
        DivExpression divExpression = new DivExpression("5    1");
        System.out.println(addExpression.interpret(interpreterEngine));
        System.out.println(subExpression.interpret(interpreterEngine));
        System.out.println(mulExpression.interpret(interpreterEngine));
        System.out.println(divExpression.interpret(interpreterEngine));
    }

}
