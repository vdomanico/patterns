package it.gof.wolf.patterns.behavioral.interpreter.impl;

public interface Expression {

    int interpret(InterpreterEngine interpreterEngine);

}
