package it.gof.wolf.patterns.behavioral.interpreter.impl;

public class SubExpression implements Expression {

    private final String expression;

    public SubExpression(String expression) {
        this.expression = expression;
    }

    @Override
    public int interpret(InterpreterEngine interpreterEngine) {
        return interpreterEngine.sub(expression);
    }
}
