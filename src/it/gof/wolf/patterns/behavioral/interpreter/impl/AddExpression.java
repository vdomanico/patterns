package it.gof.wolf.patterns.behavioral.interpreter.impl;

public class AddExpression implements Expression {

    private final String expression;

    public AddExpression(String expression) {
        this.expression = expression;
    }

    @Override
    public int interpret(InterpreterEngine interpreterEngine) {
        return interpreterEngine.add(expression);
    }
}
