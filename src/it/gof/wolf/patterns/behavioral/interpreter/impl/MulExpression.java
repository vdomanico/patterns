package it.gof.wolf.patterns.behavioral.interpreter.impl;

public class MulExpression implements Expression {

    private final String expression;

    public MulExpression(String expression) {
        this.expression = expression;
    }

    @Override
    public int interpret(InterpreterEngine interpreterEngine) {
        return interpreterEngine.mul(expression);
    }
}
