package it.gof.wolf.patterns.behavioral.interpreter.impl;

public class InterpreterEngine {

    public int add(String expression){
        String[] tokens = interpret(expression);
        int num1 = Integer.parseInt(tokens[0]);
        int num2 = Integer.parseInt(tokens[1]);
        return num1 + num2;
    }

    public int sub(String expression){
        String[] tokens = interpret(expression);
        int num1 = Integer.parseInt(tokens[0]);
        int num2 = Integer.parseInt(tokens[1]);
        return num1 - num2;
    }

    public int mul(String expression){
        String[] tokens = interpret(expression);
        int num1 = Integer.parseInt(tokens[0]);
        int num2 = Integer.parseInt(tokens[1]);
        return num1 * num2;
    }

    public int div(String expression){
        String[] tokens = interpret(expression);
        int num1 = Integer.parseInt(tokens[0]);
        int num2 = Integer.parseInt(tokens[1]);
        return num1 / num2;
    }

    private String[] interpret(String expression) {
        String string = expression.replaceAll("[^0-9]", " ");
        string = string.replaceAll("( )+", " ").trim();
        return string.split(" ");
    }

}
