package it.gof.wolf.patterns.behavioral.interpreter.impl;

public class DivExpression implements Expression {

    private final String expression;

    public DivExpression(String expression) {
        this.expression = expression;
    }

    @Override
    public int interpret(InterpreterEngine interpreterEngine) {
        return interpreterEngine.div(expression);
    }
}
