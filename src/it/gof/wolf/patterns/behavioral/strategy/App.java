package it.gof.wolf.patterns.behavioral.strategy;

import it.gof.wolf.patterns.behavioral.strategy.impl.Finance;
import it.gof.wolf.patterns.behavioral.strategy.impl.Marketing;
import it.gof.wolf.patterns.behavioral.strategy.impl.StrategyContext;

public class App {

    public static void main(String[] args) {
        new StrategyContext(new Finance()).makeDecision("buy stocks");
        new StrategyContext(new Marketing()).makeDecision("make a new brand");
    }

}
