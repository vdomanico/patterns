package it.gof.wolf.patterns.behavioral.strategy.impl;

public class Marketing implements Strategy {

    @Override
    public String make(String decision) {
       return "Marketing decision is to " + decision;
    }

}
