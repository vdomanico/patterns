package it.gof.wolf.patterns.behavioral.strategy.impl;

public class StrategyContext {

    private final Strategy strategy;

    public StrategyContext(Strategy strategy) {
        this.strategy = strategy;
    }

    public void makeDecision(String decision){
        System.out.println(this.strategy.make(decision));
    }
}
