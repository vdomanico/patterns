package it.gof.wolf.patterns.behavioral.strategy.impl;

public class Finance implements Strategy {

    @Override
    public String make(String decision) {
       return "Finance decision is to " + decision;
    }

}
