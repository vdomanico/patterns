package it.gof.wolf.patterns.behavioral.strategy.impl;

public interface Strategy {

    String make(String decision);

}
